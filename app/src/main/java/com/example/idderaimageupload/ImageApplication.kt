package com.example.idderaimageupload

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ImageApplication: Application() {
}