package com.example.idderaimageupload.network

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideClient(): OkHttpClient.Builder {
        val timeOut = 30L
        val httpClient = OkHttpClient.Builder()
        httpClient.connectTimeout(timeOut, TimeUnit.SECONDS)
        httpClient.readTimeout(timeOut, TimeUnit.SECONDS)
        httpClient.writeTimeout(timeOut, TimeUnit.SECONDS)
        httpClient.followRedirects(true)
        httpClient.followSslRedirects(true)
        httpClient.cache(null)
        return httpClient
    }

    @Provides
    @Singleton
    fun provideRetrofit(builder: OkHttpClient.Builder): Retrofit {
        val retrofitBuilder = Retrofit.Builder()
            .baseUrl("https://iddera-userprofilingapi-stage.eu-west-2.elasticbeanstalk.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(builder.build())
        return retrofitBuilder.build()
    }

    @Provides
    @Singleton
    fun provideDataService(retrofit: Retrofit) = retrofit.create(DataService::class.java)

}