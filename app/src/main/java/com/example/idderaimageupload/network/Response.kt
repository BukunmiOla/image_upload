package com.example.idderaimageupload.network

import kotlinx.serialization.Serializable


sealed class Response<T> {
    data class Success<T>(val resp: T) : Response<T>()
    data class Failure<T>(
        val exception: Throwable? = null,
        val reason: FailureReason,
        val errorResponse: NetworkResponseErrors? = null
    ) : Response<T>() {
        fun <S> failure(): Failure<S> {
            return Response.failure(exception, reason, errorResponse)
        }
    }

    companion object {
        fun <T> success(value: T) = Success(value)
        fun <T> failure(
            exception: Throwable? = null,
            reason: FailureReason = FailureReason.GENERIC_ERROR,
            errorResponse: NetworkResponseErrors? = null
        ) = Failure<T>(exception, reason, errorResponse)
    }

    fun <R> mapSuccess(block: (Success<T>) -> R): Response<R> {
        return when (this) {
            is Failure -> Failure(exception, reason, errorResponse)
            is Success ->
                try {
                    success(block(this))
                } catch (e: Throwable) {
                    failure<R>(e, FailureReason.GENERIC_ERROR)
                }
        }
    }
}

enum class FailureReason {
    OFFLINE,
    UNAUTHORIZED,
    GENERIC_ERROR,
    BAD_RESPONSE,
    CANCELLED,
    SERVICE_UNAVAILABLE,
    EMAIL_ALREADY_REGISTERED,
    PASSWORD_VALIDATION_INCORRECT,
    PASSWORDS_NOT_MATCH
}

@Serializable
data class NetworkResponseErrors(
    val errors: List<String>? = null
)

