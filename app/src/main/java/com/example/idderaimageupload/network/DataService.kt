package com.example.idderaimageupload.network

import com.example.idderaimageupload.model.RemoteDashboardResponse
import com.example.idderaimageupload.model.RemoteLoginResponse
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*


interface DataService {
    @Multipart
    @PUT("v1/settings/profile/image")
    fun uploadImage(
        @Header("Authorization") credential: String,
        @Part itemsImage: MultipartBody.Part
    ): Call<Any>

    @POST("v1/auth/login")
    fun login(@Body itemsImage: Map<String,String>): Call<RemoteLoginResponse>

    @GET("v1/dashboard/current")
    fun getDashboard(): Call<RemoteDashboardResponse>

}