package com.example.idderaimageupload.ui

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.example.idderaimageupload.model.RemoteDashboardResponse
import com.example.idderaimageupload.network.DataService
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import io.github.bukunmiola.pquiz.databinding.FragmentHomeBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : Fragment() {
    @Inject
    lateinit var service: DataService
    private lateinit var binding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getDashboardData()
    }
    private fun getDashboardData(  ){

        val call = service.getDashboard()
        call.enqueue(object : Callback<RemoteDashboardResponse> {
            override fun onResponse(call: Call<RemoteDashboardResponse>, response: Response<RemoteDashboardResponse>) {
                when (response.code()) {
                    200 -> response.body()?.data?.user?.imageUrl?.let { showPhoto(it) }
                    400 -> // Validation error
                        response.errorBody()?.string()?.let { Log.e("error",it) }
                    401 -> Log.e("","logout")
                    else -> Log.e("","Network error! ${response.code()}")
                }
            }

            override fun onFailure(call: Call<RemoteDashboardResponse>, t: Throwable) {
                Log.e("","Network error!")
            }
        })
    }

    private fun showPhoto(imageUrl: String) {
        Picasso.get().load(imageUrl).into(binding.civProfilePhoto)
    }

}