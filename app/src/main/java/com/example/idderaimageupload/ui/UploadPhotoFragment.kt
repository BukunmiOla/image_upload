package com.example.idderaimageupload.ui

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.idderaimageupload.model.RemoteLoginResponse
import com.example.idderaimageupload.network.DataService
import com.github.dhaval2404.imagepicker.ImagePicker
import dagger.hilt.android.AndroidEntryPoint
import io.github.bukunmiola.pquiz.R
import io.github.bukunmiola.pquiz.databinding.FragmentUploadPhotoBinding
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import javax.inject.Inject

@AndroidEntryPoint
class
UploadPhotoFragment : Fragment() {

    @Inject
    lateinit var service: DataService
    private lateinit var binding: FragmentUploadPhotoBinding
    private var requestCode: Int = 2222
    private var imageFilePath: String = ""
    private lateinit var imageUri: Uri

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

       binding.btnKtorUpload.setOnClickListener {

        }

        binding.btnPickPhoto.setOnClickListener {
            ImagePicker
                .with(this)
                .crop(1f, 1f) // Crop image to a square
                .compress(1024) // Final image size will be less than 1 MB
                .maxResultSize(1080, 1080) // Final image resolution will be less than 1080 x 1080
                .start(requestCode)
        }

        binding.btnRetrofitUpload.setOnClickListener {
            updateImageWithRetrofit(File(imageFilePath))
        }
    }

    fun updateImageWithRetrofit(imageFile: File) {

        val loginCall = service.login(mapOf("username" to "bukunmi@iddera.com", "password" to "Password@123"))
        loginCall.enqueue(object : Callback<RemoteLoginResponse> {
            override fun onResponse(call: Call<RemoteLoginResponse>, response: Response<RemoteLoginResponse>) {
                when (response.code()) {
                    200 -> response.body()?.data?.accessToken?.let { uploadPhoto(it, imageFile) }
                    400 -> // Validation error
                        response.errorBody()?.string()?.let { Log.e("error",it) }
                    401 -> Log.e("","logout")
                    else -> Log.e("","Network error! ${response.code()}")
                }
            }

            override fun onFailure(call: Call<RemoteLoginResponse>, t: Throwable) {
                Log.e("","Network error!")
            }
        })

    }

    private fun uploadPhoto(token: String, imageFile: File) {
        val avatarFile =
            MultipartBody.Part.createFormData("profileImage", imageFile.name, RequestBody.create(MultipartBody.FORM,imageFile))

        val call = service.uploadImage("Bearer $token", avatarFile)
        call.enqueue(object : Callback<Any> {
            override fun onResponse(call: Call<Any>, response: Response<Any>) {
                when (response.code()) {
                    200 -> showUpdatedImage()
                    400 -> // Validation error
                        response.errorBody()?.string()?.let { Log.e("error",it) }
                    401 -> Log.e("","logout")
                    else -> Log.e("","Network error! ${response.code()}")
                }
            }

            override fun onFailure(call: Call<Any>, t: Throwable) {
                Log.e("","Network error!")
            }
        })
    }

    private fun showUpdatedImage() {
        findNavController().navigate(R.id.action_uploadPhotoFragment_to_homeFragment)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && data != null) {
            imageUri = data.data!!
            imageFilePath = data.getStringExtra("extra.file_path")!!
            Log.d("","onActivityResult URI $imageUri")

            if (requestCode == requestCode) {
                binding.civProfilePhoto.setImageURI(data.data!!)
            }
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Log.e("", ImagePicker.getError(data))
        } else {
            Log.e("", "Error occurred!")
        }
    }

}
