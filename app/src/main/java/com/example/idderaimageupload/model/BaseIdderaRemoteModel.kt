package com.example.idderaimageupload.model

import kotlinx.serialization.Serializable

@Serializable
data class BaseIdderaRemoteModel<T>(
    val data: T? = null,
    val errors: List<String>? = null,
    val message: String,
    val status: String,
    val successful: Boolean? = false
)
