package com.example.idderaimageupload.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

typealias RemoteDashboardResponse = BaseIdderaRemoteModel<DashboardResponseData>

@Serializable
data class DashboardResponseData(
    @SerialName("user") val user: DashboardUser?,
    @SerialName("consultationCount") val consultationCount: ConsultationCount?,
    @SerialName("activeSubscription") var activeSubscription: Subscription?
)

@Serializable
data class DashboardUser(
    @SerialName("username") val username: String?,
    @SerialName("referralCode") val referralCode: String?,
    @SerialName("email") val email: String,
    @SerialName("userType") val userType: String?,
    @SerialName("memberSince") val memberSince: String?,
    @SerialName("firstName") val firstName: String,
    @SerialName("lastName") val lastName: String,
    @SerialName("gender") val gender: String?,
    @SerialName("dateOfBirth") val dateOfBirth: String?,
    @SerialName("maritalStatus") val maritalStatus: String?,
    @SerialName("emailVerified") val emailVerified: Boolean?,
    @SerialName("phoneNumberVerified") val phoneNumberVerified: Boolean?,
    @SerialName("address") val address: MemberAddress?,
    @SerialName("phoneNumber") val phoneNumber: String?,
    @SerialName("emergencyContact") val emergencyContact: EmergencyContact?,
    @SerialName("imageUrl") val imageUrl: String?,
    @SerialName("hasCompletedWatson") val hasCompletedWatson: Boolean?,
    @SerialName("hasPinSet") val hasPinSet: Boolean?,
    @SerialName("coursesCompleted") val hasCompletedCourses: Boolean?
)

@Serializable
data class ConsultationCount(
    @SerialName("total") val total: Int,
    @SerialName("upcoming") val upcoming: Int,
    @SerialName("past") val past: Int
)

@Serializable
data class MemberAddress(
    @SerialName("lga") val lga: String?,
    @SerialName("state") val state: String?,
    @SerialName("homeAddress") val homeAddress: String?
)

@Serializable
data class EmergencyContact(
    @SerialName("firstName") var firstName: String?,
    @SerialName("lastName") var lastName: String?,
    @SerialName("email") var email: String?,
    @SerialName("phoneNumber") var phoneNumber: String?,
    @SerialName("relationship") var relationship: String?

)
