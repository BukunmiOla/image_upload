package com.example.idderaimageupload.model

import com.example.idderaimageupload.network.Response
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

typealias RemoteLoginResponse = BaseIdderaRemoteModel<LoginResponse>

@Serializable
data class LoginResponse(
    @SerialName("accessToken")
    val accessToken: String?,
    @SerialName("expiresIn")
    val expiresIn: Int?,
    @SerialName("jti")
    val jti: String?,
    @SerialName("refreshToken")
    val refreshToken: String?,
    @SerialName("scope")
    val scope: String?,
    @SerialName("tokenType")
    val tokenType: String?
)
