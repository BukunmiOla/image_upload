package com.example.idderaimageupload.model

import kotlinx.serialization.Serializable

@Serializable
data class Subscription(
    val id: Int = 0,
    var plan: Plan = Plan(),
    val reference: String = "",
    val startDate: String? = "",
    val endDate: String? = "",
    val status: String = "",
    val totalAmount: Double = 0.0,
    val transactionStatus: String = ""
)

@Serializable
data class Plan(
    val amount: Int = 0,
    val disabled: Boolean = false,
    val id: Int = 0,
    var name: String = "",
    val period: String = ""
)
